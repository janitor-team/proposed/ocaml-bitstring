Source: ocaml-bitstring
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>,
 Mehdi Dogguy <mehdi@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml-nox,
 ocaml-findlib,
 ocaml-dune,
 libppx-tools-versioned-ocaml-dev,
 libmigrate-parsetree-ocaml-dev,
 libstdlib-shims-ocaml-dev,
 libounit-ocaml-dev,
 dh-ocaml,
 time
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-bitstring
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-bitstring.git
Homepage: https://bitstring.software/

Package: libbitstring-ocaml-dev
Architecture: any
Depends: ${ocaml:Depends}, ${misc:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Suggests: ocaml-findlib
Description: Erlang-style bitstrings for OCaml (development files)
 Bitstring adds Erlang-style bitstrings and matching over bitstrings as a syntax
 extension and library for OCaml.
 .
 You can use this module to both parse and generate binary formats, files and
 protocols.
 .
 Bitstring handling is added as primitives to the language, making it simple to
 use.
 .
 Bitstring handles integers, strings, sub-bitstrings, big-, little- and
 native-endianness, signed and unsigned types, variable-width fields, fields
 with arbitrary bit alignment.
 .
 This package contains header and OCaml library.

Package: libbitstring-ocaml
Architecture: any
Depends: ${ocaml:Depends}, ${misc:Depends}, ${shlibs:Depends}
Provides: ${ocaml:Provides}
Description: Erlang-style bitstrings for OCaml (runtime)
 Bitstring adds Erlang-style bitstrings and matching over bitstrings as a syntax
 extension and library for OCaml.
 .
 You can use this module to both parse and generate binary formats, files and
 protocols.
 .
 Bitstring handling is added as primitives to the language, making it simple to
 use.
 .
 Bitstring handles integers, strings, sub-bitstrings, big-, little- and
 native-endianness, signed and unsigned types, variable-width fields, fields
 with arbitrary bit alignment.
 .
 This package contains only the shared runtime stub libraries.
